from django.contrib.auth.decorators import login_required
from django.urls import include, path
from .views import BlogView, CreatePostView, TagView

app_name = 'blog'
urlpatterns = [
    path('post/create', login_required(CreatePostView.as_view()), name='create_post'),
    path('<str:username>', login_required(BlogView.as_view()), name='view_blog'),
    path('tags/<str:tag>', TagView.as_view(), name='tag'),
]
