from django.forms import ModelForm
from .models import Post


class PostForm(ModelForm):
    class Meta:
        model = Post
        exclude = ('tags',)

    @classmethod
    def from_post_data(cls, post_data, user_id):
        """Create a PostForm from a post request.
        """
        form_data = dict(
            user=user_id
        )

        # using **post_data or form_data.update results in values being list of str rather than str
        for k, v in post_data.items():
            form_data[k] = v

        return cls(form_data)
