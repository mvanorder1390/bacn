from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

GENDER = (('man', 'Man'), ('woman', 'Woman'))


class UserProfile(models.Model):
    user = models.OneToOneField(User, null=True, related_name="profile",
                                verbose_name=_('User'), on_delete=models.CASCADE)
    gender = models.CharField(
        max_length=40, blank=True, verbose_name=_('Gender'), choices=GENDER)
    avatar = models.ImageField(
        upload_to='userprofiles2/avatars', blank=True, verbose_name=_('Avatar'))
    email_is_verified = models.BooleanField(
        default=False, verbose_name=_('Email is verified'))

    class Meta:
        verbose_name = _('User profile')
        verbose_name_plural = _('User profiles')

    def __str__(self):
        return "User profile: %s" % self.user.username

    def get_absolute_url(self):
        return reverse('blog:view_blog', args=[str(self.user.username)])
