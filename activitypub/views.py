import re

from allauth.socialaccount.models import SocialAccount
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites import shortcuts
from django.urls import resolve, reverse
from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.http import HttpResponseNotAllowed
from django.utils.datastructures import MultiValueDictKeyError

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import ActivityPubSite, UserActor


class JsonLdApiView(APIView):

    def dispatch(self, request, *args, **kwargs):
        # Since rest_framework doesn't support activity+json requests, a workaround is necessary
        # until it does
        if re.match(r'application/(activity|ld)\+json', request.META.get('HTTP_ACCEPT', '')):
            request.META['HTTP_ACCEPT'] = 'application/json'
        return super().dispatch(request, *args, **kwargs)


class ActivityPubApiView(JsonLdApiView):
    """
    https://www.w3.org/TR/activitypub/#retrieving-objects
    """

    media_type = 'application/activity+json'
    json_ld_profile = "https://www.w3.org/ns/activitystreams"


class InstanceView(ActivityPubApiView):

    def get(self, request, *args, **kwargs):
        ap_domain = shortcuts.get_current_site(request).activitypub.domain
        instance = {
            'uri': ap_domain,
            'title': ap_domain,
            'description': '',
            'email': '',
            'version': '',
            'urls': {
            },
            'languages': 'en',
            'contact_account': '',
        }
        return Response({}, status=status.HTTP_501_NOT_IMPLEMENTED)


class ProfileView(ActivityPubApiView):

    def get(self, request, *args, **kwargs):
        if request.META['REQUEST_METHOD'] != 'GET':
            return HttpResponseNotAllowed(request.META['REQUEST_METHOD'])

        # Get an account if specified, or try to get account that's logged in
        username = kwargs.get('username')
        if username is None:
            user = self.request.user
        else:
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                return Response({
                    'code': 404,
                    'status': 'Account {} not found'.format(kwargs['username'])
                }, status=status.HTTP_404_NOT_FOUND)

        actor, created = UserActor.objects.get_or_create(user=user)
        ap_domain = shortcuts.get_current_site(request).activitypub.domain

        return Response(actor.as_json_ld(ap_domain), status=status.HTTP_200_OK)


class WebfingerView(APIView):
    media_type = 'application/jrd+json'

    def dispatch(self, request, *args, **kwargs):
        # Since rest_framework doesn't support jrd+json requests, a workaround is necessary until
        # it does
        if 'HTTP_ACCEPT' in request.META and\
           request.META['HTTP_ACCEPT'] == 'application/jrd+json':
            request.META['HTTP_ACCEPT'] = 'application/json'
        return super(WebfingerView, self).dispatch(request, *args, **kwargs)

    def get(self, request, format=None):
        try:
            resource = request.GET['resource'].lower()
        except MultiValueDictKeyError:
            return Response({
                'code': 422,
                'status': 'Unable to determine resource type'
            }, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        # Try to split resource key and value
        try:
            resource_type, resource_name = resource.split(':')
        except ValueError:
            return Response({
                'code': 422,
                'status': 'Unable to determine resource type: {}'.format(request.GET['resource'])
            }, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        # Make sure it's a supported resource type(only acct at the moment)
        if resource_type != 'acct':
            return Response({
                'code': 422,
                'status': 'Unsupported resource type: {}'.format(resource_type)
            }, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        site = shortcuts.get_current_site(request)
        if not ActivityPubSite.objects.filter(site=site):
            return Response({
                'code': 500,
                'status': 'No activitypub has been configured for site related to {}'.format(resource_name)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        ap_domain = site.activitypub.domain

        # Seperate the user and domain, and make sure the domain is correct.
        username, domain = resource_name.split('@')
        if domain != ap_domain:
            return Response({
                'code': 404,
                'status': 'Account {} not found'.format(resource_name)
            }, status=status.HTTP_404_NOT_FOUND)

        # Try to look up the user
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return Response({
                'code': 404,
                'status': 'Account {} not found'.format(resource_name)
            }, status=status.HTTP_404_NOT_FOUND)

        # If everything passed, present the user
        user_url = "https://{}/user/{}".format(ap_domain, user.username)
        data = {
            "subject": resource,
            "links": [
                {
                    "rel": "self",
                    "type": "application/activity+json",
                    "href": user_url
                }
            ],
            "aliases": [
                user_url
            ],
        }

        return Response(data, status=status.HTTP_200_OK)


class WellknownNodeinfoView(JsonLdApiView):
    """
    Tested by activitypub.test.test_views.Nodeinfo.test_node_info
    """
    media_type = 'application/jrd+json'

    def get(self, request, format=None):
        site = shortcuts.get_current_site(request)
        if not ActivityPubSite.objects.filter(site=site):
            return Response({
                'code': 500,
                'status': 'No activitypub has been configured for site.'
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        data = {
            "links": [
                {
                    "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0",
                    "href": f"https://{site.activitypub.domain}{reverse('activitypub:nodeinfo', args=['2.0'])}"
                }
            ]
        }

        return Response(data, status=status.HTTP_200_OK)


class NodeinfoView(JsonLdApiView):
    """
    Tested by activitypub.test.test_views.Nodeinfo.test_node_info
    """
    media_type = 'application/jrd+json'

    def get(self, request, version, format=None):
        return Response(shortcuts.get_current_site(request).activitypub.nodeinfo, status=status.HTTP_200_OK)
